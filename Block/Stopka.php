<?php


namespace Kowal\Stopka\Block;

class Stopka extends \Magento\Framework\View\Element\Template
{
    protected $_scopeConfig;

    /**
     * Stopka constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    )
    {

        $this->_scopeConfig = $scopeConfig;
        parent::__construct($context, $data);
    }


    /**
     * @return int
     */
    public function getColumns()
    {
        return (int)$this->getConfig('uklad/general/ilosc_kolumn');
    }

    /**
     * @param $path
     * @return string
     */
    public function getConfig($path)
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;
        return (string)$this->_scopeConfig->getValue($path, $storeScope);
    }

    /**
     * @param $i
     * @return string
     */
    public function getContentBlock($i)
    {
        return (string)$this->getLayout()
            ->createBlock('Magento\Cms\Block\Block')
            ->setBlockId($this->getConfig('uklad/stopka/kolumna_' . $i))
            ->toHtml();

    }

    /**
     * @param $i
     * @return string
     */
    public function getContentHeader($i)
    {
        $myCmsBlock = $this->getLayout()
            ->createBlock('Kowal\Stopka\Block\Cms\Block')
            ->setBlockId($this->getConfig('uklad/stopka/kolumna_' . $i));
        return (string)$myCmsBlock->getTitle();
    }

}
